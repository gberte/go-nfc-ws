**Description**

This little shit is an adaptation to https://github.com/taglme/nfcuid to create a local webservice to return de TagID readed throug a Acr122 nfc reader.

**Instalation (Ubuntu)**
1) Download package from https://packages.ubuntu.com/bionic/amd64/libpcsclite-dev/download
2) sudo dpkg -i libpcsclite-dev_1.8.23-1_amd64.deb
3) Install dependecies: go get -u github.com/ebfe/scard
4) Run the project "go run main.go"

**Use it**
1) Open browser
2) Access http://localhost:3000, wait for it...
3) Read the tagID

**Javascript Example**
```
<html>
<body>
<button onclick="fetchNFCID()">Leer tagid</button>
<script>
    var loadingNFC=false;
    function fetchNFCID(){
        if(!loadingNFC){
            console.log("Loading tagid...");
            loadingNFC==true;
            const http = new XMLHttpRequest();
            http.open("GET","http://localhost:3000");
            http.send();
            http.onreadystatechange = (e)=>{
                if (http.status === 200) {
                    condole.log(http.responseText);
                }else{
                    console.log("Error "+http.status +", try again!");
                }
                loadingNFC=false;
            }
        }
    }
</script>
</body>
</html>
```
