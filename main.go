package main

import (
	"fmt"
	"log"
	"net/http"
	"os"

	"github.com/ebfe/scard"
)

var loading = false
var readers []string
var ctx *scard.Context
var err error

func errorExit(err error) {
	fmt.Println(err)
	os.Exit(1)
}

func waitUntilCardPresent(ctx *scard.Context, readers []string) (int, error) {
	rs := make([]scard.ReaderState, len(readers))
	for i := range rs {
		rs[i].Reader = readers[i]
		rs[i].CurrentState = scard.StateUnaware
	}

	for {
		for i := range rs {
			if rs[i].EventState&scard.StatePresent != 0 {
				return i, nil
			}
			rs[i].CurrentState = rs[i].EventState
		}
		err := ctx.GetStatusChange(rs, -1)
		if err != nil {
			return -1, err
		}
	}
}

func waitUntilCardRelease(ctx *scard.Context, readers []string, index int) error {
	rs := make([]scard.ReaderState, 1)

	rs[0].Reader = readers[index]
	rs[0].CurrentState = scard.StatePresent

	for {

		if rs[0].EventState&scard.StateEmpty != 0 {
			return nil
		}
		rs[0].CurrentState = rs[0].EventState

		err := ctx.GetStatusChange(rs, -1)
		if err != nil {
			return err
		}
	}
}

func handler(w http.ResponseWriter, r *http.Request) {

	// List available readers
	readers, err = ctx.ListReaders()
	if err != nil {
		fmt.Println(err)
	}

	if len(readers) > 0 {
		w.Header().Set("Access-Control-Allow-Origin", "*")

		//fmt.Println("Waiting for a Card")
		index, err := waitUntilCardPresent(ctx, readers)
		if err != nil {
			fmt.Println(err)
		}

		// Connect to card
		//fmt.Println("Connecting to card in ", readers[index])
		card, err := ctx.Connect(readers[index], scard.ShareExclusive, scard.ProtocolAny)
		if err != nil {
			fmt.Println(err)
		}
		defer card.Disconnect(scard.ResetCard)

		var cmd = []byte{0xFF, 0xCA, 0x00, 0x00, 0x00}

		rsp, err := card.Transmit(cmd)
		if err != nil {
			fmt.Println(err)
		}
		uid := string(rsp[0:7])
		uidS := fmt.Sprintf("%x", uid)

		if uidS == "63000000000000" {
			fmt.Println("Try")
			fmt.Fprintf(w, "Try again.")
		} else {
			fmt.Println(uidS)
			fmt.Fprintf(w, uidS)
		}
	} else {
		fmt.Println("No readers found.")
	}
}

func bootstrap() {
	// Establish a context
	ctx, err = scard.EstablishContext()
	if err != nil {
		fmt.Println(err)
	}
	defer ctx.Release()

	//Initializing the server
	http.HandleFunc("/", handler)
	log.Fatal(http.ListenAndServe(":3000", nil))
}

func main() {
	bootstrap()
}
